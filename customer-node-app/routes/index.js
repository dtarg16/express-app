var express = require('express');
const { getCustomerSql } = require('../services/customer-mysql');
var router = express.Router();
const { getCustomers, getCustomerById, getCustomersBySearch } = require('../services/customer-mysql');
const { getProducts, getProductById, getProductsBySearch } = require('../services/product-mysql');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Customer App' });
});

router.get('/about', function(req, res, next) {
  res.render('index', { title: 'About' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.get('/dashboard', async function(req, res, next) {
  res.render('dashboard', { title: 'Dashbord', customers: await getCustomers()});  
});

router.get('/customers', async function(req, res, next) {
  res.render('customers', { title: 'Customers', data: await getCustomers()});
});

router.get('/customers/add', function(req, res, next) {
  res.render('add-customer', { title: 'Add Customer'});
});

router.get('/customers/edit/:id', async function(req, res, next) {
  const customer = await getCustomerById(req.params.id)
  res.render('edit-customer', { title: 'Update Customer', record: customer});
});

router.get('/customers/search/:field/:searchWord', async function(req, res, next) {
  res.render('customers', { title: 'Customers', data:await getCustomersBySearch(req.params.field, req.params.searchWord)});
});

router.get('/products', async function(req, res, next) {
  res.render('products', { title: 'Products', data: await getProducts()});
});

router.get('/products/add', function(req, res, next) {
  res.render('add-product', { title: 'Add Product'});
});

router.get('/products/edit/:id', async function(req, res, next) {
  const product = await getProductById(req.params.id)
  res.render('edit-product', { title: 'Update Product', record: product});
});

router.get('/products/search/:field/:searchWord', async function(req, res, next) {
  res.render('products', { title: 'Products', data:await getProductsBySearch(req.params.field, req.params.searchWord)});
});

module.exports = router;
