var express = require('express');
var router = express.Router();
const {getProducts, addProduct, getProductById, updateProduct, deleteProduct} = require('../services/product-mysql.js')

router.get('/',  async (req, res) => {
  let products = await getProducts()
  res.send(products);
});

router.get('/:id',  async (req, res) => {
  let product = await getProductById(req.params.id)
  res.send(product);
});

router.post('/',  async (req, res) => {
  let product = req.body;
  await addProduct(product);
  res.send({result:'ok', msg:'Record added successfully'});
});

router.put('/',  async (req, res) => {
  let product = req.body;
  await updateProduct(product)
  res.send({result:'ok', msg:'Record updated successfully'});
});

router.delete('/', async (req, res) => {
  let product = req.body;
  await deleteProduct(product);
  res.send({result:'ok', msg:'Record deleted successfully'});
});

module.exports = router;
