var express = require('express');
var router = express.Router();
const {getCustomers, addCustomer, updateCustomer, deleteCustomer, getCustomerById} = require('../services/customer-mysql')


router.get('/',  async (req, res) => {
  let customers = await getCustomers()
  res.send(customers);
});

router.get('/:id', async (req, res) => {
  let customer = await getCustomerById(req.params.id)
  res.send(customer);
});

router.post('/',  async (req, res) => {
  let customer = req.body;
  await addCustomer(customer);
  res.send({result:'ok', msg:'Record added successfully'});
});

router.put('/',  async (req, res) => {
  let customer = req.body;
  await updateCustomer(customer)
  res.send({result:'ok', msg:'Record updated successfully'});
});

router.delete('/',  async (req, res) => {
  let customer = req.body;
  await deleteCustomer(customer);
  res.send({result:'ok', msg:'Record deleted successfully'});
});

module.exports = router;
