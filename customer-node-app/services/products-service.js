var products = [
    {id:1, name: 'Phone', company: 'Apple', code: "1", date: '11/08/22', city: 'Tbilisi', country: 'Georgia'},
    {id:2, name: 'Phone', company: 'Samsung', code: "2", date: '12/08/22', city: 'Tbilisi', country: 'Georgia'},
    {id:3, name: 'Phone', company: 'Google', code: "3", date: '13/08/22', city: 'Tbilisi', country: 'Georgia'},
];

const getProducts = () => (products);

const getProductById = (id) => {
  let temp = products.filter((product)=>(product.id == id));
  if(temp.length > 0){
    return temp[0];
  }
  return [];
};

const addProduct = (product) => {
    products.push(product);
};

const updateProduct = (productToUpdate) => {
    products.map((product, index)=>{
        if(product.id == productToUpdate.id){
            products[index] = productToUpdate
        }
    })
};

const deleteProduct = (productToDelete) => { 
    products = products.filter((product)=>(product.id != productToDelete.id));
};

module.exports = {getProducts, addProduct, updateProduct, deleteProduct, getProductById};
