var customers = [
    {id:1, name: 'Vivek', email: 'vivek@abcd.com', phone: '1234567899', address: 'India'},
    {id:2, name: 'Rama', email: 'rama@abcd.com', phone: 'wwwww', address: 'Asia'},
    {id:3, name: 'David', email: 'david@gmail.com', phone: '1234', address: 'Georgia'},
];

const getCustomers = () => (customers);

const getCustomerById = (id) => {
  let temp = customers.filter((customer)=>(customer.id == id));
  if(temp.length > 0){
    return temp[0];
  }
  return [];
};

const addCustomer = (customer) => {
    customers.push(customer);
};

const updateCustomer = (customerToUpdate) => {
    customers.map((customer, index)=>{
        if(customer.id == customerToUpdate.id){
            customers[index] = customerToUpdate
        }
    })
};

const deleteCustomer = (customerToDelete) => { 
    customers = customers.filter((customer)=>(customer.id != customerToDelete.id));
};

module.exports = {getCustomers, addCustomer, updateCustomer, deleteCustomer, getCustomerById};
